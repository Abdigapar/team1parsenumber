﻿using System.Collections.Generic;

namespace Team1ParseNumber
{
    public  class RegionTypes
    {
        static Dictionary<int, string> _regionType = new Dictionary<int, string>(16)
        {
            {01, "Astana"},
            {02, "Almaty"},
            {03, "AkmolaRegion"},
            {04, "AktobeRegion"},
            {05, "AlmatyRegion"},
            {06, "AtyrauRegion"},
            {07, "WestKazakhstanRegion"},
            {08, "JambylRegion"},
            {09, "KaragandaRegion"},
            {10, "KostanayRegion"},
            {11, "KyzylordaRegion"},
            {12, "MangistauRegion"},
            {13, "SouthKazakhstanRegion"},
            {14, "PavlodarRegion"},
            {15, "NorthKazakhstanRegion"},
            {16, "EastKazakhstanRegion"}
        };
        //public const string Astana = "01";
        //public const string Almaty = "02";
        //public const string AkmolaRegion = "03";
        //public const string AktobeRegion = "04";
        //public const string AlmatyRegion = "05";
        //public const string AtyrauRegion = "06";
        //public const string WestKazakhstanRegion = "07";
        //public const string JambylRegion = "08";
        //public const string KaragandaRegion = "09";
        //public const string KostanayRegion = "10";
        //public const string KyzylordaRegion = "11";
        //public const string MangistauRegion = "12";
        //public const string SouthKazakhstanRegion = "13";
        //public const string PavlodarRegion = "14";
        //public const string NorthKazakhstanRegion = "15";
        //public const string EastKazakhstanRegion = "16";

        //public  readonly string[] AvailableRegion =
        //{
        //    Astana, Almaty, AkmolaRegion, AktobeRegion, AlmatyRegion, AtyrauRegion, WestKazakhstanRegion, JambylRegion,
        //    KaragandaRegion, KostanayRegion, KyzylordaRegion, MangistauRegion, SouthKazakhstanRegion, PavlodarRegion,
        //    NorthKazakhstanRegion, EastKazakhstanRegion
        //};
    }
}

