﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Team1ParseNumber
{
    public class Program
    {
        static readonly List<CarNumber> carNumbers = new List<CarNumber>();
        static Dictionary<string, string> _regionType = new Dictionary<string, string>(16)
        {
            {"01", "Astana"},
            {"02", "Almaty"},
            {"03", "AkmolaRegion"},
            {"04", "AktobeRegion"},
            {"05", "AlmatyRegion"},
            {"06", "AtyrauRegion"},
            {"07", "WestKazakhstanRegion"},
            {"08", "JambylRegion"},
            {"09", "KaragandaRegion"},
            {"10", "KostanayRegion"},
            {"11", "KyzylordaRegion"},
            {"12", "MangistauRegion"},
            {"13", "SouthKazakhstanRegion"},
            {"14", "PavlodarRegion"},
            {"15", "NorthKazakhstanRegion"},
            {"16", "EastKazakhstanRegion"}
        };
        static void Main(string[] args)
        {
            var again = true;

            carNumbers.Add(new CarNumber(1, "kz", "123", "aba", "01"));
            carNumbers.Add(new CarNumber(2, "kz", "313", "da", "02"));
            carNumbers.Add(new CarNumber(3, "kz", "313", "drd", "03"));
            carNumbers.Add(new CarNumber(4, "kz", "456", "sps", "04"));
            carNumbers.Add(new CarNumber(5, "kz", "888", "wa", "05"));
            carNumbers.Add(new CarNumber(6, "kz", "923", "wad", "06"));
            carNumbers.Add(new CarNumber(7, "kz", "000", "ddd", "11"));
            GetAll();
            Console.WriteLine();
            while (again)
            {
                Console.WriteLine("Выберите что вы хотите сделать со списком номеров : " +
                                  "\nНажмите 1 : Введите номер для парсинга" +
                                  "\nНажмите 2 : Что-бы Добавить номер" +
                                  "\nНажмите 3 : Что-бы Удалитть номер" +
                                  "\nНажмите 4 : Что-бы Найти номер" +
                                  "\nНажмите 5 : Найти зеркальные номера" +
                                  "\nНажмите 6 : Найти крутые номера" +
                                  "\nНажмите 7 : Найти регион"+
                                  "\nНажмите 8 : Сортировать");

                var operation = Console.ReadLine();
                switch (operation)
                {
                    case "1":
                        ParseNumber();
                        break;
                    case "2":
                        Add();
                        break;
                    case "3":
                        Delete();
                        break;
                    case "4":
                        Search();
                        break;
                    case "5":
                        SearchBestNumberSerialNumber();
                        break;
                    case "6":
                        SearchBestNumberIndex();
                        break;
                    case "7":
                        SearchRegion();
                        break;
                    case "8":
                        SortNumber();
                        break;
                    default:
                        Console.WriteLine("Не правильный ввод ");
                        break;
                }

                GetAll();
                Console.WriteLine("-------------------");
                Console.WriteLine("Что бы продолжить нажмите y, выйти нажмите e");
                var yesOrNo = Console.ReadLine();
                if (yesOrNo == "y".ToLower() || yesOrNo == "e".ToLower())
                {
                    again = true;
                }
                else
                {
                    again = false;
                }
            }
            Console.Read();
        }

        static void GetAll()
        {
            Console.WriteLine("Список номеров :");
            {
                foreach (var f in carNumbers)
                {
                    Console.WriteLine($" f: {f.Id} | {f.Country}{f.Index}{f.SerialNumber}{f.Region}");
                }
            }
        }

        static void ParseNumber()
        {
            var again = false;
            string caNumber = string.Empty;

            while (!again)
            {
                Console.WriteLine("введите номер");
                string number = Console.ReadLine()?.ToUpper();

                char firstSymbol = number[0];
                char secondSymbol = number[1];

                if (firstSymbol != 'K' && secondSymbol != 'Z')
                {
                    Console.WriteLine("Номера Республики Казахстан начинаются с префикса: kz , пожалуйста введите заново");
                }
                else
                {
                    caNumber = number;
                    again = true;
                }
            }

            if (caNumber.Length == 10)
            {
                caNumber = caNumber.Insert(2, "-").Insert(6, "-").Insert(10, "-");
                Console.WriteLine("Номер физичекого лица:  " + caNumber);
                Console.WriteLine("-------------------");
            }
            else if (caNumber.Length == 9)
            {
                caNumber = caNumber.Insert(2, "-").Insert(6, "-").Insert(9, "-");
                Console.WriteLine("Номер юридического лица:  " + caNumber);
                Console.WriteLine("-------------------");
            }
            else
            {
                Console.WriteLine("Неверный формат номера, пожалуйста введите заново. Номер состоит из: " +
                                  "\n1) Кода страны: kz " +
                                  "\n2) Серинный номер из 3 чисел " +
                                  "\n3) Индекс дя физ лиц: 3 буквы | для юр лиц: 2 буквы " +
                                  "\n4) Kод текущего 01-16");
            }
        }

        static void Add()
        {
            int id = carNumbers.LastOrDefault().Id;

            Console.WriteLine("Введите Страну : ");
            string country = Console.ReadLine();

            Console.WriteLine("Введите Index : ");
            string index = Console.ReadLine();

            Console.WriteLine("Введите серийный номер : ");
            string serialNumber = Console.ReadLine();
            if (serialNumber.Length == 2)
            {
                Console.WriteLine("ЮрЛицо");
            }
            else if (serialNumber.Length == 3)
            {
                Console.WriteLine("ФизЛицо");
            }
            else
            {
                Console.WriteLine("Серинный номер должен состоять из 2 или 3 букв");
                return;
            }

            Console.WriteLine("Введите код региона : ");
            var region = Console.ReadLine();

            var newNumber = new CarNumber(++id, country, index, serialNumber, region);

            carNumbers.Add(newNumber);
        }

        static void Delete()
        {
            Console.WriteLine("Ввдите Id номера которого вы хотите удалить : ");

            var id = Convert.ToInt32(Console.ReadLine());

            var car = carNumbers.FirstOrDefault(c => c.Id == id);
            carNumbers.Remove(car);
        }

        static void SearchBestNumberSerialNumber()
        {
            Console.WriteLine("Список зеркальных номеров: ");

            var find = carNumbers.OrderBy(c => c.SerialNumber).ToList();

            foreach (var f in find)
            {
                //for (int i = 0; i < f.SerialNumber.Length / 2; i++)
                //{
                //    if (f.SerialNumber[i] == f.SerialNumber[f.SerialNumber.Length - i - 1])
                //    {
                //        Console.WriteLine($" f: {f.Id} | {f.Country}{f.Index}{f.SerialNumber}{f.Region}");
                //    }
                //}

                if (f.Index.Substring(0, 1) == f.Index.Substring(2, 1) )
                {
                    Console.WriteLine($" : {f.Id} | {f.Country}{f.Index}{f.SerialNumber}{f.Region}");
                }
            }
        }

        static void SearchBestNumberIndex()
        {
            Console.WriteLine("Список крутых номеров: ");

            var find = carNumbers.OrderBy(c => c.Index).ToList();

            foreach (var f in find)
            {
                //for (int i = 0; i < f.Index.Length / 2; i++)
                //{
                //    if (f.Index[i] == f.Index[f.Index.Length - i - 1])
                //    {
                //        Console.WriteLine($" f: {f.Id} | {f.Country}{f.Index}{f.SerialNumber}{f.Region}");
                //    }
                //}
                //kz111ddd01
                if (f.Index.Substring(0, 1) == f.Index.Substring(2, 1) || f.SerialNumber.Substring(0, 1) == f.SerialNumber.Substring(2, 1))
                {
                    Console.WriteLine($" : {f.Id} | {f.Country}{f.Index}{f.SerialNumber}{f.Region}");
                }
            }
        }

        static void SearchRegion()
        {
            Console.WriteLine("Введите код региона");
            var idRegion = Console.ReadLine();

            var regionType = _regionType.Where(c => c.Key == idRegion);

            var findCar = carNumbers.Where(c => c.Region == regionType.ToString());

            foreach (var f in regionType)
            {
                foreach (var f1 in findCar)
                {
                    Console.WriteLine($" : {f1.Id} | {f1.Country}{f1.Index}{f1.SerialNumber}{f1.Region}");
                    Console.WriteLine($"{f.Key}-{f.Value} ");
                }
            }
        }

        static void Search()
        {
            Console.WriteLine("Введите Index Номера : ");
            string index = Console.ReadLine().ToLower();
            var findIndex = carNumbers.Where(c => c.Index == index);
            foreach (var F in findIndex)
            {
                if (F.Index == index)
                {
                    Console.WriteLine();

                    Console.WriteLine("Введите serialNumber : ");
                    string serialNumber = Console.ReadLine();
                    var findSerialNumber = carNumbers.Where(c => c.SerialNumber == serialNumber);
                    foreach (var f in findSerialNumber)
                    {
                        Console.WriteLine($" f: {f.Id} | {f.Country}{f.Index}{f.SerialNumber}{f.Region}");
                    }
                }
            }
        }
        static void SortNumber()
        {
            var face = carNumbers.ToList();

            foreach (var fizface in face)
            {
                if (fizface.SerialNumber.Length == 3)
                {
                    Console.WriteLine($" Физ лицо : Id{fizface.Id} | {fizface.Country}{fizface.Index}{fizface.SerialNumber}{fizface.Region}");
                }
            }

            foreach (var YurFace in face)
            {
                if (YurFace.SerialNumber.Length == 2)
                {
                    Console.WriteLine($" ЮрЛицо   : Id{YurFace.Id} | {YurFace.Country}{YurFace.Index}{YurFace.SerialNumber}{YurFace.Region}");
                }
            }
        }
    }
}
