﻿namespace Team1ParseNumber
{
    public class CarNumber
    {
        public CarNumber(int id, string country, string index, string serialNumber, string region)
        {
            Id = id;
            Country = country;
            Index = index;
            SerialNumber = serialNumber;
            Region = region;
        }

        public int Id { get; set; }
        public string Country { get; set; }
        public string Index { get; set; }
        public string SerialNumber { get; set; }
        public string Region { get; set; }

        private string[] _city = new[] { "Astana", "Almaty", "AkmolaRegion"};

    }
}